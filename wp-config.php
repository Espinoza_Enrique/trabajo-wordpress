<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'tiendavirtual' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         ',:}21wRci(8hDaI4HE!7JZ5FSmmf>&J3cH~=E=W^T&S94639z+;+N3vu#Rm/y h2' );
define( 'SECURE_AUTH_KEY',  'slIV#=*^2=~c,**Dxf9{a[NemDP2*gS #,+^;V3,e;K1Dh]BbqKDNvP9-h>9V5A[' );
define( 'LOGGED_IN_KEY',    'K@;tD[i@YHkSR<sC &%A59!|jt};OnW>nhBIkLykQhK<6NlvmSB);TmP^a^-ysKF' );
define( 'NONCE_KEY',        'JDp`+uDt,v6z+aTiT > b-u^!Y4im7OuNGrc% 5oh/CpOj+Wk8pfOQ*=)_,`!@.q' );
define( 'AUTH_SALT',        'd/Ai.7,P^9m}baenk2LIcc_>8Ic)/4l<3&y@bcAc[TuL!D: cn4d~@O[^:6.wD!G' );
define( 'SECURE_AUTH_SALT', '8F8!c~;3J`}YqOZmZim2MDDW*a!qp/oCzt5+uJM{uTm0EFy=2*^rT0_igWG&$rvt' );
define( 'LOGGED_IN_SALT',   'oo-BmZ_PvolKylRd{R,UfA0Jj2Z>YqYRD%FhPcgXrY<3R]?J?,tz!e-/ F/as`,S' );
define( 'NONCE_SALT',       'XzQPv=q]M=.s&Pwmg<s6.&,{GM;5xA|#hhj;uK9cb6,OqdVwi3MI[2+vRVB(}[YM' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'de_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
